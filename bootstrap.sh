#!/usr/bin/env sh

set -ex

test -n "$SHELLDEBUG" && set -x

TEST_HOST="${TEST_HOST:-127.0.0.1}"

get_exposed_port() {
  docker-compose port "$@" | cut -d: -f2
}

wait_for() {
  echo "Waiting for $*"
  wait-for -t 60 -s 5 "$@"
}

rm -rf build
mkdir build

docker-compose pull mongo rabbitmq
docker-compose down --timeout 0 --remove-orphans --volumes

docker-compose up -d mongo rabbitmq

wait_for "http://guest:guest@${TEST_HOST}:$(get_exposed_port rabbitmq 15672)/api/healthchecks/node"

RABBITMQ="${TEST_HOST}:$(get_exposed_port rabbitmq 15672)" \
  prep-it

echo Environment variables:
tee build/test-environment << EOF
export AMQP_URL=amqp://guest:guest@${TEST_HOST}:$(get_exposed_port rabbitmq 5672)/%2F
export MONGO_URL=mongodb://root:rootpassword@${TEST_HOST}:$(get_exposed_port mongo 27017)
export ASYNC_TEST_TIMEOUT=10.0
EOF

. ./build/test-environment

echo Bootstrap complete

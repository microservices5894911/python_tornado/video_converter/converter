import os
from unittest import TestCase, mock

from sprockets.mixins import amqp
from tornado import testing

import tests.helpers
from converter import app


class ApplicationTests(tests.helpers.ResetEnvironmentMixin,
                       testing.AsyncTestCase):
    def test_that_environment_defaults_to_development(self):
        application = app.Application()
        self.assertEqual(application.settings['environment'], 'development')

    def test_that_environment_is_set_from_envvar(self):
        os.environ['ENVIRONMENT'] = 'WHATEVER'
        application = app.Application()
        self.assertEqual(application.settings['environment'], 'WHATEVER')

    def test_that_service_defaults_to_auth(self):
        application = app.Application()
        self.assertEqual(application.settings['service'], 'converter')

    @testing.gen_test
    async def test_create_db_pool_fails(self):
        with mock.patch.object(amqp, 'install', side_effect=Exception):
            with self.assertRaises(SystemExit):
                application = app.Application()
                await application.on_start(application, self.io_loop)


class RunTests(tests.helpers.ResetEnvironmentMixin, TestCase):
    def setUp(self):
        super().setUp()
        self._patched = mock.patch('converter.app.sprockets.http')
        self.sprockets_http = self._patched.start()

    def tearDown(self):
        self._patched.stop()
        super().tearDown()

    def test_that_single_process_is_enabled(self):
        app.run()
        self.assertTrue(self.sprockets_http.run.called)
        posn, kwargs = self.sprockets_http.run.call_args_list[0]
        self.assertEqual(kwargs['settings']['number_of_procs'], 1)

    def test_that_xheaders_are_enabled(self):
        app.run()
        self.assertTrue(self.sprockets_http.run.called)
        posn, kwargs = self.sprockets_http.run.call_args_list[0]
        self.assertEqual(kwargs['settings']['xheaders'], True)

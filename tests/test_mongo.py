import os
import unittest

import motor

from converter import mongo

TEST_URL = 'mongodb://127.0.0.1:27017'
TEST_DB = 'test'


class FakeApplication(object):
    """Fake tornado application to test install method."""
    pass


class TestMongoInstall(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        # Stash off mongo config environment variables.
        if 'MONGO_URL' in os.environ:
            cls.original_mongo_url = os.environ['MONGO_URL']
            del os.environ['MONGO_URL']
        if 'MONGO_DATABASE' in os.environ:
            cls.original_mongo_db = os.environ['MONGO_DATABASE']
            del os.environ['MONGO_DB']

    @classmethod
    def tearDownClass(cls):
        # Restore original mongo config environment variables.
        if hasattr(cls, 'original_mongo_url'):
            os.environ['MONGO_URL'] = cls.original_mongo_url
        if hasattr(cls, 'original_mongo_db'):
            os.environ['MONGO_DATABASE'] = cls.original_mongo_db

    def test_install_using_kwargs(self):
        app = FakeApplication()
        result = mongo.install(app, mongo_url=TEST_URL, mongo_database=TEST_DB)
        self.assertTrue(result)
        self.assertIsInstance(app.motor_client,
                              motor.motor_asyncio.AsyncIOMotorClient)
        self.assertIsInstance(app.mongo,
                              motor.motor_asyncio.AsyncIOMotorDatabase)

    def test_install_using_environment_vars(self):
        app = FakeApplication()

        try:
            os.environ['MONGO_URL'] = TEST_URL
            os.environ['MONGO_DATABASE'] = TEST_DB

            result = mongo.install(app)
        finally:
            del os.environ['MONGO_URL']
            del os.environ['MONGO_DATABASE']

        self.assertTrue(result)
        self.assertIsInstance(app.motor_client,
                              motor.motor_asyncio.AsyncIOMotorClient)
        self.assertIsInstance(app.mongo,
                              motor.motor_asyncio.AsyncIOMotorDatabase)

    def test_install_motor_connection_exists(self):
        app = FakeApplication()
        app.motor_client = 1
        with self.assertRaises(mongo.MongoProgrammingError):
            mongo.install(app, mongo_url=TEST_URL, mongo_database=TEST_DB)

    def test_install_mongo_exists(self):
        app = FakeApplication()
        app.mongo = 1
        with self.assertRaises(mongo.MongoProgrammingError):
            mongo.install(app, mongo_url=TEST_URL, mongo_database=TEST_DB)

    def test_install_missing_url(self):
        app = FakeApplication()

        with self.assertRaises(mongo.MongoConfigError):
            mongo.install(app, mongo_database=TEST_DB)

    def test_install_using_invalid_url(self):
        app = FakeApplication()
        with self.assertRaises(mongo.MongoConfigError):
            mongo.install(app,
                          mongo_url='mongodb://localhost?w=1',
                          mongo_database=TEST_DB)

    def test_install_missing_database(self):
        app = FakeApplication()

        with self.assertRaises(mongo.MongoConfigError):
            mongo.install(app, mongo_url=TEST_URL)

    def test_install_using_invalid_database(self):
        app = FakeApplication()
        with self.assertRaises(mongo.MongoConfigError):
            mongo.install(app, mongo_url=TEST_URL, mongo_database=1)


class TestMongoShutdown(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        # Stash off mongo config environment variables.
        if 'MONGO_URL' in os.environ:
            cls.original_mongo_url = os.environ['MONGO_URL']
            del os.environ['MONGO_URL']
        if 'MONGO_DATABASE' in os.environ:
            cls.original_mongo_db = os.environ['MONGO_DATABASE']
            del os.environ['MONGO_DB']

    @classmethod
    def tearDownClass(cls):
        # Restore original mongo config environment variables.
        if hasattr(cls, 'original_mongo_url'):
            os.environ['MONGO_URL'] = cls.original_mongo_url
        if hasattr(cls, 'original_mongo_db'):
            os.environ['MONGO_DATABASE'] = cls.original_mongo_db

    def test_shutdown(self):
        app = FakeApplication()
        mongo.install(app, mongo_url=TEST_URL, mongo_database=TEST_DB)
        mongo.shutdown(app)
        self.assertIsNone(getattr(app, 'motor_client', None))

    def test_shutdown_without_client(self):
        app = FakeApplication()
        mongo.shutdown(app)
        self.assertIsNone(getattr(app, 'motor_client', None))

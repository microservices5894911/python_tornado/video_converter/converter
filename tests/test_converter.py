import json
import os
from functools import partial
from urllib.parse import quote

from tornado import iostream

from tests import helpers


async def raw_producer(filename, write):
    if not filename:
        return
    chunk_size = 1024 * 1024 * 1
    with open(filename, 'rb') as f:
        while True:
            chunk = f.read(chunk_size)
            if not chunk:
                break
            try:
                write(chunk)
            except iostream.StreamClosedError:
                break
            finally:
                del chunk


class ConverterTests(helpers.TestCase):
    def setUp(self):
        super().setUp()

    def test_converter(self):
        filename = './test_videos/test_video1.mp4'
        headers = {'Content-Type': 'application/octet-stream'}
        producer = partial(raw_producer, filename)
        url_path = quote(os.path.basename(filename))

        response = self.fetch('/converter?file_name=%s' % url_path,
                              method='PUT',
                              headers=headers,
                              body_producer=producer)
        self.assertEqual(200, response.code)
        res = json.loads(response.body.decode('utf8'))
        self.assertIsNotNone(res['download_id'])
        self.assertEqual(res['file_name'], 'test_video1.mp4')
        os.environ["ASYNC_TEST_TIMEOUT"] = str(5)

    def test_converter_without_file_name(self):
        filename = './test_videos/test_video1.mp4'
        headers = {'Content-Type': 'application/octet-stream'}
        producer = partial(raw_producer, filename)
        response = self.fetch('/converter',
                              method='PUT',
                              headers=headers,
                              body_producer=producer)
        self.assertEqual(400, response.code)
        self.assertEqual(response.reason, 'file name not found in query param')

    def test_converter_without_video_file(self):
        headers = {'Content-Type': 'application/octet-stream'}
        producer = partial(raw_producer, None)
        filename = './test_videos/test_video1.mp4'
        url_path = quote(os.path.basename(filename))
        response = self.fetch('/converter?file_name=%s' % url_path,
                              method='PUT',
                              headers=headers,
                              body_producer=producer)
        self.assertEqual(400, response.code)
        self.assertEqual(response.reason, 'video file not attached')

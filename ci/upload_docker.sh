#!/usr/bin/env sh
set -e

rm -fr dist
./setup.py sdist

DOCKER_IMAGE=$DOCKER_USER_NAME/converter

if [ -n "$CI_COMMIT_TAG" ]; then
    DOCKER_TAG="$CI_COMMIT_TAG"
elif [ -n "$CI_COMMIT_SHA" ]; then
    DOCKER_TAG="$(echo "$CI_COMMIT_SHA" | cut -c1-7)"
else
    echo 'error: this script must be run in a GitLab CI pipeline' >&2
    exit 1
fi

docker build \
    --pull \
    --tag "$DOCKER_IMAGE:$DOCKER_TAG" \
    --tag "$DOCKER_IMAGE:latest" \
    --label "url=$CI_PROJECT_URL" \
    --label "version=$DOCKER_TAG" \
    .

docker login -u "$DOCKER_USER_NAME" -p "$DOCKER_HUB_TOKEN"
docker push "$DOCKER_IMAGE:$DOCKER_TAG"
docker push "$DOCKER_IMAGE:latest"

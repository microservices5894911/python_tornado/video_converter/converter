"""
Mongo Mixin
===========

A request handler mixin for making MongoDB queries with Motor.

Sets two data members on the application:
    motor_client - an instance of the motor.motor_asyncio.AsyncIOMotorClient
    mongo - a convenience handle to a motor.motor_asyncio.AsyncIOMotorDatabase

Can be configured by keyword args on the install method or via the following
environment variables:

    MONGO_URL: mongo connection url.
    MONGO_DATABASE: The mongo database to use.

"""
import logging
import os

from motor.motor_asyncio import AsyncIOMotorClient
from pymongo import errors

MAX_CONNECTION_ATTEMPTS = 3
"""Number of MongoDB connection attempts to try before giving up."""

RECONNECT_DELAY = 1.0
"""Number of seconds to wait between connection retry attempts."""

LOGGER = logging.getLogger(__name__)


class MongoConfigError(Exception):
    """Raised when invalid MongoDB configuration is detected."""
    pass


class MongoProgrammingError(Exception):
    """Raised when a handler attribute name collision occurs."""
    pass


class MongoSystemError(Exception):
    """Base class for MongoDB related operational system errors"""
    pass


class MongoUnexpectedError(MongoSystemError):
    """Raised when unexpected MongoDB error occurs"""
    msg = "MDB UNEXPECTED EXCEPTION"


class MongoConnectionError(MongoSystemError):
    """Raised when MongoDB server cannot be reached"""
    msg = "MDB CONNECTION ERROR"


class MongoTimeoutError(MongoSystemError):
    "Raised when a request takes too long."
    msg = "MDB REQUEST TIMEOUT"


def install(application, **kwargs):
    """
    Initialize the Mongo connection layer.

    :param tornado.web.Application application: the application to
        install the client into.
    :param kwargs: keyword parameters to pass to the
        :class:`motor.motor_asyncio.AsyncIOMotorClient` initializer.
    :keyword str mongo_url: The mongo connection url
    :keyword str mongo_database: The mongo database to use.

    :return: True if the client was installed by this call. False otherwise.
    :raises `broadcast_scheduling.mixins.mongo.MongoConfigError`:
    :raises `broadcast_scheduling.mixins.mongo.MongoProgrammingError`:

    """
    if getattr(application, 'motor_client', None) is not None:
        LOGGER.warning('motor client is already installed')
        raise MongoProgrammingError('motor_client already exists')

    if getattr(application, 'mongo', None) is not None:
        LOGGER.warning('mongo database is already installed')
        raise MongoProgrammingError('mongo_database already exists')

    connection_url = kwargs.get('mongo_url', os.getenv('MONGO_URL'))
    if not connection_url:
        LOGGER.error('missing required mongo_url config')
        raise MongoConfigError("MONGO_URL")

    database = kwargs.get('mongo_database', os.getenv('MONGO_DATABASE'))
    if not database:
        LOGGER.error('missing required mongo_database config')
        raise MongoConfigError("MONGO_DATABASE")

    try:
        LOGGER.debug("mongo connection url - %s", connection_url)
        application.motor_client = AsyncIOMotorClient(connection_url)
    except errors.InvalidURI:
        LOGGER.error('invalid mongo_url: %s', connection_url)
        raise MongoConfigError("MONGO_URL")

    try:
        LOGGER.debug("mongo connection database - %s", database)
        application.mongo = application.motor_client[database]
    except TypeError:
        LOGGER.error('invalid mongo_database: %s', database)
        raise MongoConfigError("MONGO_DATABASE")

    return True


def shutdown(application):
    """Shutdown the Mongo connection layer and delete motor_client from app.
    :param tornado.web.Application application: the application
    """
    LOGGER.info('Shutting down mongo connections')
    client = getattr(application, 'motor_client', None)
    if client:
        client.close()
        delattr(application, 'motor_client')
    LOGGER.info('Mongo connection shutdown complete')

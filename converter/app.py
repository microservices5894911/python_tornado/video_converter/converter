import os
import sys

import pkg_resources
import sprockets.http
import sprockets.http.app
from sprockets.mixins import amqp
from sprockets.mixins.mediatype import content, transcoders
from tornado import web

from converter import handlers, mongo, version


class Application(sprockets.http.app.Application):
    def __init__(self, **kwargs):
        urls = [
            web.url('/', handlers.OpenApiHandler),
            web.url(r'/(openapi.yaml)', handlers.OpenApiHandler),
            web.url(r'/status', handlers.StatusHandler),
            web.url(r'/converter', handlers.ConverterHandler)
        ]
        service = os.environ.get('SERVICE', 'converter')
        kwargs.update({'service': service, 'version': version})
        super().__init__(urls,
                         static_path=pkg_resources.resource_filename(
                             __package__, 'static'),
                         template_path=pkg_resources.resource_filename(
                             __package__, 'templates'),
                         **kwargs)

        environment = os.environ.get('ENVIRONMENT', 'development')
        self.settings.update({
            'environment': environment,
            'amqp_url': os.environ['AMQP_URL']
        })

        self.settings['static_path'] = os.environ.get(
            'STATIC_PATH', pkg_resources.resource_filename(__name__, 'static'))
        self.settings['template_path'] = pkg_resources.resource_filename(
            __name__, 'templates')

        content.set_default_content_type(self, 'application/json', 'utf-8')

        json = transcoders.JSONTranscoder()
        json.dump_options['sort_keys'] = True
        content.add_transcoder(self, json)

        self.mongo = None
        self.ready_to_serve = False
        self._rabbitmq_ready = False

        self.before_run_callbacks.append(self._before_run)
        self.on_start_callbacks.append(self.on_start)
        self.on_shutdown_callbacks.append(self.on_shutdown)

    @staticmethod
    def _before_run(app, _ioloop=None):
        """
        Called by sprockets.http before IOLoop is running.
        :param Application app: application object
        :param tornado.ioloop.IOLoop _ioloop: IOLoop that we are running on
        """
        mongo.install(app,
                      mongo_database=os.getenv('MONGO_DATABASE', 'converter'))

    async def on_start(self, app, io_loop, *_):
        try:
            amqp.install(
                app, io_loop, **{
                    'enable_confirmations': False,
                    'on_ready_callback': app.on_rabbitmq_ready,
                    'on_unavailable_callback': app.on_rabbitmq_unavailable
                })
        except Exception as e:
            self.logger.exception(e)
            sys.exit(1)
        self.ready_to_serve = True

    async def on_shutdown(self, app, *_):
        mongo.shutdown(app)

    def on_rabbitmq_ready(self, caller=None):
        self.logger.info('rabbit connection established.')
        self._rabbitmq_ready = True

    def on_rabbitmq_unavailable(self, caller=None):
        self.logger.info('rabbit connection lost')
        self._rabbitmq_ready = False


def run():
    sprockets.http.run(Application,
                       settings={
                           'number_of_procs': 1,
                           'xheaders': True
                       })

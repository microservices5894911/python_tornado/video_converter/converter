import json
import os
import tempfile
import uuid

import pkg_resources
import tornado.web
from motor.motor_asyncio import AsyncIOMotorGridFSBucket
from sprockets.mixins import amqp

from converter import helpers, version

MAX_STREAMED_SIZE = 1024 * 1024 * 1024


class OpenApiHandler(helpers.RequestHandler):
    """Services up the index page and OpenAPI doc"""
    def get(self, *args, **kwargs):
        template = args[0] if args else "index.html"
        if args and args[0] == "openapi.yaml":
            self.set_header("Content-Type", "text/yaml")
        self.set_header("Access-Control-Allow-Origin", "*")
        self.render(
            template,
            **{
                "host": self.request.host,
                "scheme": self.request.protocol,
                "settings": self.settings,
                "version": pkg_resources.get_distribution(__package__).version,
                "name": "Converter",
            },
        )


class StatusHandler(helpers.RequestHandler):
    """
    Simple heartbeat handler.

    Doesn't check anything beyond IOLoop responsiveness.

    """
    def get(self):
        if self.application.ready_to_serve \
                and self.application._rabbitmq_ready:
            status = 200
            text = 'ok'
        else:
            status = 503
            text = 'maintenance'
        self.set_status(status)
        self.send_response({
            'service': self.settings['service'],
            'version': version,
            'status': text
        })


@tornado.web.stream_request_body
class ConverterHandler(amqp.PublishingMixin, helpers.RequestHandler):
    def initialize(self):
        super().initialize()
        self.tmp = tempfile.NamedTemporaryFile(delete=False)

    def prepare(self):
        self.request.connection.set_max_body_size(MAX_STREAMED_SIZE)

    def data_received(self, chunk):
        self.tmp.write(chunk)

    async def put(self):
        file_name = self.get_query_argument('file_name', None)
        if not file_name:
            self.send_error(400, reason='file name not found in query param')
            return

        if os.path.getsize(self.tmp.name) == 0:
            self.send_error(400, reason='video file not attached')
            return

        fs = AsyncIOMotorGridFSBucket(self.application.motor_client.video)
        file_id = str(uuid.uuid4())
        async with fs.open_upload_stream_with_id(
                file_id=file_id,
                filename=file_name,
                metadata={"contentType": "text/octet-stream"}) as gridin:
            with open(self.tmp.name, 'rb') as f:
                for piece in self.read_in_chunks(f):
                    await gridin.write(piece)

        os.unlink(self.tmp.name)
        message = {
            'video_fid': str(file_id),
            'video_name': file_name,
            'mp3_fid': None
        }

        await self.amqp_publish('converter', 'video', json.dumps(message),
                                {'content_type': 'application/json'})
        self.send_response({
            'download_id': str(file_id),
            'file_name': file_name
        })

    def read_in_chunks(self, file_object, chunk_size=MAX_STREAMED_SIZE):
        """Lazy function (generator) to read a file piece by piece.
        Default chunk size: 1k."""
        while True:
            data = file_object.read(chunk_size)
            if not data:
                break
            yield data

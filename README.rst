=================
Converter Service
=================

This service saves video file to db and publish message to queue for audio extraction

:Repository:    `<https://gitlab.com/microservices5894911/python_tornado/video_converter/converter>`_
:Docker Tags:   `<https://hub.docker.com/repository/docker/mvijayaragavan/converter/>`_


Environment Variables
=====================

Required
--------
- ``AMQP_URL``:  The AMQP connection string used to emit events and RPC messages.
- ``MONGO_URL``: Connection string used to connect to the mongo database.


Quickstart Development Guide
============================

Setting up your environment
---------------------------
.. code-block:: bash

   $ python3.9 -m venv env
   $ . ./env/bin/activate
   $ pip install -e '.[docs,tests]'
   $ ./bootstrap.sh
   $ . ./build/test-environment

Testing the application in dev
------------------------------
.. code-block:: bash

	(env) $ flake8
	(env) $ coverage run && coverage report

Running Locally
---------------
.. code-block:: bash

	(env) $ converter

Building
--------
.. code-block:: bash

	(env) $ docker build .

Using in Another Development Environment
----------------------------------------
This service publishes a docker image to `https://hub.docker.com/repository/docker/mvijayaragavan/converter/`_ so
integrating it into an existing docker-based development environment is simple.

At the time of writing the following docker-compose snippet is what is required:

.. code-block:: yaml

    %YAML 1.2
    ---
    version: "3.0"
    services:
      converter:
        image: mvijayaragavan/converter:latest
        ports:
          - 8000
        environment:
          AMQP_URL: amqp://guest:guest@rabbitmq:5672/%2F
          MONGO_URL: mongodb://root:rootpassword@mongo:27017

      mongo:
        image: mongo:latest
        ports:
          - 27017
        environment:
          MONGO_INITDB_ROOT_USERNAME: root
          MONGO_INITDB_ROOT_PASSWORD: rootpassword

      rabbitmq:
        image: rabbitmq:3-management-alpine
        ports:
          - 5672
          - 15672
